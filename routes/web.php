<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TransactionController;
use App\Models\Transaction;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/install', function(){
    Artisan::call('migrate');
    Artisan::call('db:seed');
    return "OK";
});
// Route::redirect('/', '/dashboard', 301);

Route::prefix('/')->middleware('auth')->group(function(){

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('/product', ProductController::class);
    Route::resource('/category', CategoryController::class);
    Route::resource('/customer', CustomerController::class);

    Route::get('/transaction/create', [TransactionController::class, 'create'])
    ->name('transaction.create');
    Route::get('/transaction', [TransactionController::class, 'index'])
    ->name('transaction.index');
    Route::get('/transaction/{id}/show', [TransactionController::class, 'show'])
    ->name('transaction.show');

    Route::post('/transaction/save-troli', [TransactionController::class, 'saveTroli'])
    ->name('transaction.save-troli');
    Route::post('/transaction/save-transaksi', [TransactionController::class, 'saveTransaksi'])
    ->name('transaction.save-transaksi');
    Route::delete('/transaction/{id}', [TransactionController::class, 'destroy'])
    ->name('transaction.destroy');

});
