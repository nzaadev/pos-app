<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Troli extends Model
{
    use HasFactory;
    protected $fillable = ['product_id', 'qty'];

    function product(){
        return $this->belongsTo(Product::class);
    }
}
