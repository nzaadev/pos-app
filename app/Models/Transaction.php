<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $guarded = [];

    function customer(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    function orders(){
        return $this->hasMany(Order::class, 'transaksi_id');
    }
}
