<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // list product
        $data = Product::all();
        return view('product.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create', ['categories' => Category::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request);
        $this->validate($request, [
            'name' => 'required|min:3',
            'stock' => 'required|integer',
            'price' => 'required|integer',
            'category_id' => 'required|integer'
        ],[
            'name.required' => 'Nama produk harus diisi',
            'name.min' => 'Nama produk minimal 3 karakter',
            'price.required' => 'Harga harus diisi',
            'stock.required' => 'Stock produk harus diisi',
            'category_id.required' => 'category belum dipilih',
            ]);

        Product::create([
            'name' => $request->name,
            'price' => $request->price,
            'stock' => $request->stock,
            'category_id' => $request->category_id
        ]);

        Alert::success('Berhasil', 'produk berhasil disimpan');
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('product.edit', ['data' => Product::find($id), 'categories' => Category::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'stock' => 'required|integer',
            'price' => 'required|integer',
            'category_id' => 'required|integer'
        ],[
            'name.required' => 'Nama produk harus diisi',
            'name.min' => 'Nama produk minimal 3 karakter',
            'price.required' => 'Harga harus diisi',
            'stock.required' => 'Stock produk harus diisi',
            'category_id.required' => 'category belum dipilih',
            ]);

        Product::where('id', $id)
            ->update([
                'name' => $request->name,
                'price' => $request->price,
                'stock' => $request->stock,
                'category_id' => $request->category_id
        ]);

        Alert::success('Berhasil', 'produk berhasil diperbarui');
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        Alert::success('Berhasil', 'produk berhasil dihapus');
        return redirect()->route('product.index');
    }
}
