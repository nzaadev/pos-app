<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Product;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('customer.index', ['data' => Customer::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'phone' => 'required',
            'email' => 'required',
        ],[
            'name.required' => 'Nama produk harus diisi',
            'name.min' => 'Nama produk minimal 3 karakter',
            'phone.required' => 'no. wa harus diisi',
            'email.required' => 'email harus diisi',

            ]);


        $id = Customer::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email
        ]);

        if(!empty($request->redir)){
            return redirect($request->redir.'?customer_id='.$id->id);
        }
        return redirect()->back()->with('message','customer has been saved');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('customer.edit', ['data' => Customer::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'phone' => 'required',
            'email' => 'required',
        ],[
            'name.required' => 'Nama produk harus diisi',
            'name.min' => 'Nama produk minimal 3 karakter',
            'phone.required' => 'no. wa harus diisi',
            'email.required' => 'email harus diisi',

            ]);


        Customer::where('id', $id)
            ->update([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email
        ]);

        return redirect()->back()->with('message','customer has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer::destroy($id);
        return redirect()->back()->with('message','customer has been deleted');

    }
}
