<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;


class DashboardController extends Controller
{
    function index(){
        $data = [
            'tproduct' => Product::count(),
            'tcategory' => Category::count(),
            'tcustomer' => Customer::count(),
            'ttransaction' => Transaction::count(),
        ];
        return view('dashboard.index', compact('data'));
    }
}
