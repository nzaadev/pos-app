<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\Troli;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class TransactionController extends Controller
{
    //
    function create(){

        // dd();
        return view('transaction.create', [
        'data' => Product::all(),
        'troli' => Troli::with('product')->get(),
        'customer' => Customer::all()
    ]);
    }

    function index(){
        // dd(Transaction::find(1)->customer);
        return view('transaction.index', ['data' => Transaction::all()]);
    }

    function show($id){
        return view('transaction.show', ['data' => Transaction::find($id)]);
    }

    function saveTroli(Request $request){
        $data = Troli::updateOrCreate(
            ['product_id' => $request->product_id],
            ['qty' => DB::raw('qty+1')
            ]
        );
        return redirect()->back();
    }


    function saveTransaksi(Request $request){

        // dd($request->all());
        if($request->hasAny('addQty')) {
            Troli::where('product_id', $request->addQty)
                ->update([
                    'qty' => DB::raw('qty+1')
            ]);

            return redirect()->back();
        }

        if($request->hasAny('subQty')) {
            Troli::where('product_id', $request->subQty)
                ->update([
                    'qty' => DB::raw('qty-1')
            ]);

            return redirect()->back();
        }

        if($request->hasAny('clear')) {
            Troli::truncate();
            Alert::success('Berhasil','Troli berhasil dihapus');
            return redirect()->back();
        }

        $this->validate($request, [
            'customer_id' => 'required',
            'product_id' => 'required',
            'qty' => 'required'
        ],
    [
        'customer_id.required' => 'konsumen belum dipilih',
        'product_id.required' => 'produk belum dipilih',
    ]);

        $p = Product::select(['id', 'stock'])->whereIn('id', $request->product_id)->get();

        $no = 0;
        $msg = "";
        foreach($p as $d) {
            if($request->qty[$no] > $d->stock ) {
                $msg = "Out of stock";
            }
            $no++;
        }

        if($msg != "") {
            Alert::error('Upps','Stok produk tidak cukup');
            return redirect()->back();
        }



        $idTransaksi = Transaction::create([
            'customer_id' => $request->customer_id
        ]);


        for($prod = 0; $prod < count($request->product_id); $prod++ )
        {
            $product = Product::find($request->product_id[$prod]);
            $qty = $request->qty[$prod];

            $product->stock = DB::raw('stock-'.$qty);
            $product->save();

            Order::create([
                'quantity' => $qty,
                'product_id' => $request->product_id[$prod],
                'transaksi_id' => $idTransaksi->id,
                'product_name' => $product->name,
                'price' => $product->price
            ]);
        }

        Troli::truncate();
        return redirect(route('transaction.index'))->with('message','Transaksi berhasil dismpan');

    }

    function destroy($id) {
        Transaction::destroy($id);
        Alert::success('Berhasil','Transaksi berhasil dihapus');
        return redirect(route('transaction.index'));
    }
}
