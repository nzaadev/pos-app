@extends('layout.app')

@section('content')
    <!-- Content Row -->

    <div class="row">

        <div class="col-xl-8 col-lg-7">

            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Daftar produk</h6>
                    <a href="{{ route('product.create') }}" class="btn btn-primary btn-sm">Tambah</a>

                </div>
                <!-- Card Body -->
                <div class="card-body">
                   <table id="dataTable" class="table table-bordered" width="100%" tablespacing="0">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama produk</th>
                                <th>Kategori</th>
                                <th>Stok</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                           @forelse ($data as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->category_id }}</td>
                                <td>{{ $item->stock }}</td>
                                <td>
                                    <a href="{{ route('product.edit', $item->id) }}" class="btn btn-warning">Edit</a>
                                    <form style="display: inline" action="{{ route('product.destroy', $item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                           @empty
                            @php
                                Alert::warning('Data produk kosong', 'silahkan tambah produk terlebih dahulu');
                            @endphp
                               <tr>
                                   <td style="text-align: center" colspan="5"><b>Data Kosong</b></td>
                               </tr>
                           @endforelse
                        </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
@endsection
