@extends('layout.app')

@section('content')
<div class="row">
    <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Tambah Produk</h6>

            </div>
            <!-- Card Body -->
            <div class="card-body">
                <form method="POST" action="{{ route('product.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="">Nama produk</label>
                        <input value="{{ old('name') }}" name="name" type="text" class="form-control @error('name')
                            is-invalid
                        @enderror">
                        @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Harga</label>
                        <input value="{{ old('price') }}" name="price" type="number" class="form-control @error('price')
                            is-invalid
                        @enderror">
                        @error('price')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Stok</label>
                        <input value="{{ old('stock') }}" name="stock" type="number" class="form-control @error('stock')
                            is-invalid
                        @enderror">
                        @error('stock')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                   <div class="form-group">
                       <label for="">Kategori</label>
                    <select name="category_id" class="form-select form-control @error('category_id')
                    is-invalid
                @enderror">
                @error('category_id')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror">
                        <option value="">--- pilih kategori ---</option>
                        @forelse ($categories as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @empty
                            <option value="">-- tambah kategori terlebih dahulu!! --</option>
                        @endforelse
                    </select>
                    @error('category_id')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                   </div>



                    <button class="d-block btn btn-primary" type="submit">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
