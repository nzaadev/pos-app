@extends('layout.app')

@section('content')
<div class="row">
    <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Ubah produk</h6>

            </div>
            <!-- Card Body -->
            <div class="card-body">
                <form method="POST" action="{{ route('product.update',  $data->id ) }}">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="">Nama produk</label>
                        <input value="{{ old('name') ? old('name') : $data['name'] }}" name="name" type="text" class="form-control @error('name')
                            is-invalid
                        @enderror">
                        @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="">Harga</label>
                        <input value="{{ old('price') ? old('price') : $data['price'] }}" name="price" type="number" class="form-control @error('price')
                            is-invalid
                        @enderror">
                        @error('price')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Stok</label>
                        <input value="{{ old('stock') ? old('stock') : $data['stock'] }}" name="stock" type="number" class="form-control @error('stock')
                            is-invalid
                        @enderror">
                        @error('stock')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                   <div class="form-group">
                       <label for="">Kategori</label>
                    <select name="category_id" class="form-select form-control @error('category_id')
                    is-invalid
                @enderror">
                @error('category_id')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror">
                        <option value="">--- Pilih kategori ---</option>
                        @forelse ($categories as $item)
                            <option {{ $item->id == $data['category_id'] ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->name }}</option>
                        @empty
                            <option value="">-- silahlan tambah kategori terlebih dahulu --</option>
                        @endforelse
                    </select>
                    @error('category_id')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                   </div>



                    <button class="btn btn-primary" type="submit">Perbarui produk</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
