@extends('layout.app')

@section('content')
    <!-- Content Row -->

    <div class="row">

        <!-- Area Chart -->
        <div class="col-xl-8 col-lg-7">
            @if (Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
            @endif

            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Daftar konsumen</h6>
                    <a href="{{ route('customer.create') }}" class="btn btn-primary btn-sm">Tambah konsumen</a>

                </div>
                <!-- Card Body -->
                <div class="card-body">
                   <table class="table table-bordered" width="100%" tablespacing="0">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama konsumen</th>
                                <th>No. Wa</th>
                                <th>Email</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                           @forelse ($data as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->phone }}</td>
                                <td>{{ $item->email }}</td>
                                <td>
                                    <a href="{{ route('customer.edit', $item->id) }}" class="btn btn-warning btn-sm">Ubah</a>
                                    <form style="display: inline" action="{{ route('customer.destroy', $item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                           @empty
                               <tr>
                                   <td style="text-align: center" colspan="5"><b>Data Kosong</b></td>
                               </tr>
                           @endforelse
                        </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
@endsection
