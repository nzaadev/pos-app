@extends('layout.app')

@section('content')
<div class="row">
    <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Tambah konsumen</h6>

            </div>
            <!-- Card Body -->
            <div class="card-body">

                <form method="POST" action="{{ route('customer.store') }}">
                    @csrf
                    <input type="hidden" name="redir" value="{{ Request::get('redir') }}">
                <div class="form-group">
                    <label for="">Nama konsumen</label>
                    <input value="{{ old('name') ? old('name') : ''}}" name="name" type="text" class="form-control @error('name')
                        is-invalid
                    @enderror">
                    @error('name')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="">No. wa</label>
                    <input value="{{ old('phone') ? old('phone') : '' }}" name="phone" type="number" class="form-control @error('phone')
                        is-invalid
                    @enderror">
                    @error('phone')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input value="{{ old('email') ? old('email') : '' }}" name="email" type="email" class="form-control @error('email')
                        is-invalid
                    @enderror">
                    @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>




                    <button class="d-block btn btn-primary" type="submit">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
