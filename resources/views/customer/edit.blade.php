@extends('layout.app')

@section('content')
<div class="row">
    <div class="col-xl-8 col-lg-7">
        @if (Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
        @endif
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Ubah data konsumen {{ $data->name }}</h6>

            </div>
            <!-- Card Body -->
            <div class="card-body">
                <form method="POST" action="{{ route('customer.update',  $data->id ) }}">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="">Name</label>
                        <input value="{{ old('name') ? old('name') : $data['name'] }}" name="name" type="text" class="form-control @error('name')
                            is-invalid
                        @enderror">
                        @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="">No. wa</label>
                        <input value="{{ old('phone') ? old('phone') : $data['phone'] }}" name="phone" type="number" class="form-control @error('phone')
                            is-invalid
                        @enderror">
                        @error('phone')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input value="{{ old('email') ? old('email') : $data['email'] }}" name="email" type="email" class="form-control @error('email')
                            is-invalid
                        @enderror">
                        @error('email')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>



                    <button class="btn btn-primary" type="submit">Perbarui</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
