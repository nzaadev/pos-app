@extends('layout.app')

@section('content')
<div class="row">

    <div class="col">

        @if (Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
        @endif

        <div class="card shadow">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between"><h6 class="font-weight-bold text-primary">Transaksi</h6></div>
            <div class="card-body">
                <table class="table table-hover table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No. Transaksi</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @forelse ($data as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->customer->name }}</td>
                                <td>{{ $item->created_at }}</td>
                                <td>
                                    <a href="{{ route('transaction.show', ['id' => $item->id]) }}" class="btn btn-primary">
                                        Show
                                    </a>
                                    <form class="d-inline-block" method="POST" action="{{ route('transaction.destroy', ['id' => $item->id]) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger ">Delete</button>

                                    </form>
                                </td>
                            </tr>
                        @empty

                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection

@section('script')

@endsection
