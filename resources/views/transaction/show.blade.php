@extends('layout.app')

@section('content')
<div class="row">

    <div class="col-6">
        <div class="card shadow">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between"><h6 class="font-weight-bold text-primary">Transaksi</h6></div>
            <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item">Transaksi ID: {{ $data->id }}</li>
                        <li class="list-group-item">Tanggal: {{ $data->created_at }}</li>
                        <li class="list-group-item">Customer: {{ $data->customer->name }}</li>
                        <li class="list-group-item">
                            Orders:
                            <br>
                            @php
                                $tmp = 0;
                            @endphp
                            @forelse ($data->orders as $item)
                            {{ $item->product_name }} {{ $item->quantity }} x {{ $item->price }} = {{ $item->quantity*$item->price }} <br>
                                @php
                                    $tmp += $item->quantity*$item->price;
                                @endphp
                            @empty


                            @endforelse

                            Total: {{ $tmp }}
                        </li>
                    </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection
