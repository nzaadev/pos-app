@extends('layout.app')

@section('content')
<div class="row">
    <div class="col-lg-6">
        @if (Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
        @endif
        <div class="card shadow">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between"><h6 class="font-weight-bold text-primary">Transaksi</h6></div>
            <div class="card-body">
                <form method="POST" action="{{ route('transaction.save-troli') }}" class="form">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Pilih Produk</label>
                                <select name="product_id" id="" class="form-control">
                                    @forelse ($data  as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                                <button class="btn btn-primary mt-3">Tambah ke troli</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-lg-6 mt-4 mt-lg-0">
        @if($errors->any())
        <div class="alert alert-danger p-0 pt-3">
            <ul>
                {!! implode('', $errors->all('<li>:message</li>')) !!}
            </ul>
        </div>
        @endif
        <div class="card shadow">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between"><h6 class="font-weight-bold text-primary">Troli</h6></div>
            <div class="card-body">
                <form method="POST" action="{{ route('transaction.save-transaksi') }}">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <div class="col-8">
                                <select name="customer_id" id="" class="form-control">
                                    <option value="">-- konsumen --</option>
                                    @forelse ($customer as $item)
                                    <option {{ Request::get('customer_id') == $item->id || old('customer_id') == $item->id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                            </div>
                            <div class="col-4">
                                <a href="{{ route('customer.create', ['redir' => route('transaction.index')]) }}" class="btn btn-sm btn-primary">tambah konsumen</a>
                            </div>
                        </div>
                    </div>

                    <ul class="list-group">
                        @php
                            $sum = 0;
                        @endphp

                        @forelse ($troli as $item)
                        @php
                            $subtotal = $item->qty * $item->product->price;
                            $sum += $subtotal;
                        @endphp
                        <input type="hidden" name="product_id[]" value="{{ $item->product->id }}">
                        <input type="hidden" name="qty[]" value="{{ $item->qty }}">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <div class="ms-2 me-auto">
                            <div class="fw-bold text-primary text-bold">{{ $item->product->name }}</div>
                            Sub: {{ number_format($subtotal,0,",",".") }} (@ {{ $item->qty }} x {{ $item->product->price }})
                            </div>
                            <div class="d-flex justify-content-around">
                                <button type="submit" name="subQty" value="{{ $item->product->id }}" class="btn btn-sm btn-primary">-</button>
                                <span class="mx-3">{{ $item->qty }}</span>
                                <button type="submit" name="addQty" value="{{ $item->product->id }}" class="btn btn-sm btn-primary">+</button>
                            </div>
                        </li>
                        @empty
                        Troli kosong
                        @endforelse
                    </ul>

                    <hr>

                    <div>
                        <div class="d-flex justify-content-between mx-1">
                            <h6 class="font-weight-bold">Total :</h6>
                            <h6 class="font-weight-bold text-primary">Rp. {{ number_format($sum,0,",",".") }}</h6>
                            <input type="hidden" name="total" id="total" value="{{ $sum }}">
                        </div>
                    </div>

                    <div>
                        <div class="d-flex justify-content-between mx-1">
                            <h6 class="font-weight-bold">Tunai :</h6>
                            <h6 class="font-weight-bold text-primary" id="tunai">-</h6>
                        </div>
                    </div>

                    <div>
                        <div class="d-flex justify-content-between mx-1">
                            <h6 class="font-weight-bold">Kembalian :</h6>
                            <h6 class="font-weight-bold text-primary" id="kembaliantxt">-</h6>
                        </div>
                    </div>


                    <div>
                        <div class="form-group">
                            <input id="kembalian" type="number" class="form-control" placeholder="uang tunai">
                        </div>
                    </div>

                    <div class="d-flex justify-content-between mb-3">
                        <a onclick="setKembalian('pas')" class="btn btn-success btn-sm">uang pas</a>
                        <a onclick="setKembalian(20000)" class="btn btn-secondary btn-sm">20.000</a>
                        <a onclick="setKembalian(50000)" class="btn btn-secondary btn-sm">50.000</a>
                        <a onclick="setKembalian(100000)" class="btn btn-secondary btn-sm">100.000</a>
                    </div>

                    <div class="row">
                        <input type="submit" class="col mr-1 mx-2 btn btn-danger" name="clear" value="Hapus troli">
                        <input type="submit" class="col btn btn-primary" value="Bayar">
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>
@endsection

@section('script')
    <script>
        // var a = document.querySelector(".sidebar");
        // a.classList.toggle('toggled');


        $("#kembalian").on("keyup", function(e){
            // if(isNaN(e.target.value)) {
            //     alert("masukkan angka");
            //     e.target.value = ""
            //     return
            // }

            var nilai = parseInt(e.target.value)
            displayKembalian(nilai)

        })

        function displayKembalian(e){

            document.querySelector("#tunai").textContent = `Rp. ${e.toLocaleString('id-ID')}`
            var s = "";
            var total = parseInt($("#total").val())
            var kembalian = e - total
            if(kembalian < 0) {
                s = '<span class="text-danger">(uang kurang)</span>'
            }
            document.querySelector("#kembaliantxt").innerHTML = `${s} Rp. ${kembalian.toLocaleString('id-ID')}`
        }

        function setKembalian(k) {
            var nilai = 0
            if(k=='pas'){
                nilai = parseInt($("#total").val())
            } else {
                nilai = k
            }

            $("#kembalian").val(nilai)
            displayKembalian(nilai)

        }


    </script>
@endsection
