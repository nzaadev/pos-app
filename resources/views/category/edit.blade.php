@extends('layout.app')

@section('content')
<div class="row">
    <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Ubah kategori</h6>

            </div>
            <!-- Card Body -->
            <div class="card-body">
                <form method="POST" action="{{ route('category.update',  $data->id ) }}">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="">Nama kategori</label>
                        <input value="{{ old('name') ? old('name') : $data['name'] }}" name="name" type="text" class="form-control @error('name')
                            is-invalid
                        @enderror">
                        @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>



                    <button class="btn btn-primary" type="submit">Perbarui</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
