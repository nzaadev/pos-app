@extends('layout.app')

@section('content')
    <!-- Content Row -->

    <div class="row">
        <div class="col-xl-8 col-lg-7">


            @if (Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
            @endif

            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Daftar kategori</h6>
                    <a href="{{ route('category.create') }}" class="btn btn-sm btn-primary">Tambah kategori</a>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                   <table class="table table-bordered" width="100%" tablespacing="0">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>


                        <tbody>
                           @forelse ($data as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    <a href="{{ route('category.edit', $item->id) }}" class="btn btn-sm btn-warning">Ubah</a>
                                    <form style="display: inline" action="{{ route('category.destroy', $item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                           @empty
                               <tr>
                                   <td style="text-align: center" colspan="5"><b>Data Kosong</b></td>
                               </tr>
                           @endforelse
                        </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
@endsection
