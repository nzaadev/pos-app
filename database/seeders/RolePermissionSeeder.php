<?php

namespace Database\Seeders;

use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Permission
        Permission::create(['name' => 'manage_product']);
        Permission::create(['name' => 'manage_category']);
        Permission::create(['name' => 'manage_transaction']);

        // role kasir
        $kasir = Role::create(['name' => 'kasir']);
        $kasir->givePermissionTo(['manage_transaction']);


        // admin
        $admin = Role::create(['name' => 'admin']);
        $admin->givePermissionTo(Permission::all());

        $user1 = User::find(1);
        $user1->assignRole($admin);

        $user2 = User::find(2);
        $user2->assignRole($kasir);




    }
}
