<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Seeder;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Iman nz',
            'email' => 'iman@gmail.com',
            'password' => bcrypt('admin')
        ]);

        User::create([
            'name' => 'Dendy',
            'email' => 'Dendy@gmail.com',
            'password' => bcrypt('admin')
        ]);
    }
}
