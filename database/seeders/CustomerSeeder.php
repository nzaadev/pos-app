<?php

namespace Database\Seeders;
use App\Models\Customer;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::create([
            'name' => 'Rizal FS',
            'phone' => '08888',
            'email' => 'rizalfs@gmail.com'
        ]);

        Customer::create([
            'name' => 'Yogga',
            'phone' => '08888',
            'email' => 'yogga@gmail.com'
        ]);

        Customer::create([
            'name' => 'Dedy',
            'phone' => '08888',
            'email' => 'deddy@gmail.com'
        ]);
    }
}
